# M07 Guestbook Applicaton

A simple guest book using Node, Express, BootStrap, EJS

## How to use
Place all the files on c:\44563\m07 folder
Open a command window in your c:\44563\m07 folder.

Run npm install to install all the dependencies in the package.json file.

Run node gbapp.js or nodemon gbapp.js to start the server.  (Hit CTRL-C to stop.)

```
> npm install
> nodemon gbapp.js
```

Point your browser to `http://localhost:8081`. 